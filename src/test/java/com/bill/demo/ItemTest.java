package com.bill.demo;

import com.bill.demo.domain.item.Item;
import com.bill.demo.domain.item.ItemType;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Customer Unit testing.
 */
public class ItemTest extends TestCase {
	/**
	 * Create the test case
	 *
	 * @param testName
	 *            name of the test case
	 */
	public ItemTest(String testName) {
		super(testName);
	}

	/**
	 * @return the suite of Item tests
	 */
	public static Test suite() {
		return new TestSuite(ItemTest.class);
	}

	/**
	 * Item Test
	 */
	public void testItem() {

		double expectedPrice = 1;

		ItemType expectedType = ItemType.GROCERY;

		Item item = new Item(expectedType, expectedPrice);

		assertEquals("testing item price", expectedPrice, item.getPrice());
		assertEquals("testing item type", expectedType, item.getType());

		expectedPrice = 2;
		item.setPrice(expectedPrice);
		assertEquals("testing item price set", expectedPrice, item.getPrice());

		expectedType = ItemType.NON_GROCERY;
		item.setType(expectedType);
		assertEquals("testing item type set", expectedType, item.getType());

	}
}
