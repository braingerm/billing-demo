package com.bill.demo.domain.bill;

import java.util.List;

import com.bill.demo.domain.customer.BaseCustomer;
import com.bill.demo.domain.item.Item;
import com.bill.demo.domain.item.ItemType;

public class Bill {

	BaseCustomer customer;

	List<Item> items;

	public Bill() {

	}

	public Bill(BaseCustomer customer, List<Item> items) {
		this.customer = customer;
		this.items = items;
	}

	public BaseCustomer getCustomer() {
		return customer;
	}

	public void setCustomer(BaseCustomer customer) {
		this.customer = customer;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	/**
	 * calculates the total bill amount to pay: <br>
	 * * for non grocery items apply the % discount <br>
	 * * for every 100 USD paid give discount of 5 USD
	 * 
	 * @return total amount to pay
	 */
	public double amountToPay() {

		final double onTotalsDiscount = 0.95;

		final double total = items.stream().map(item -> {
			if (item.getType() == ItemType.NON_GROCERY)
				return (1 - customer.discountPercentage()) * item.getPrice();
			return item.getPrice();
		}).reduce(0D, Double::sum);

		return onTotalsDiscount * total;
	}
}
