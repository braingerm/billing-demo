#### Readme

first git clone or download the repo

####  Domain Classes 
under src\main\java\com\bill\demo\domain\

* **Bill**: class representing the bill
* **Item**: class represent the item to buy having a type (item type: grocery or else)
* **BaseCustomer**: Abstract Customer Base Class
* **Employee**: Extending BaseCustomer for the employee rate 
* **Affiliate**: Extending BaseCustomer for the affiliate rate
* **Customer**: Extending BaseCustomer for a normal customer rate (older than 2 years or not)

**Bill Amount To Pay Calculation In the Image Below:**
![bill_total.png](https://bitbucket.org/braingerm/billing-demo/raw/93a97b203c928125469ae331cd4c0de25df782c1/bill_total.png)


#### Test Classes
under src\test\java\com\bill\demo\

* **ItemTest**: Item Related Unit Testing
* **CustomerTest**: Customers Related Unit Testing 
* **BillTest**: Bill Related Unit Testing

**Bill (creation of a bill for a customer(s) with items ) Unit Testing Explained in the Image Below**
![bill_test.png](https://bitbucket.org/braingerm/billing-demo/raw/93a97b203c928125469ae331cd4c0de25df782c1/bill_test.png)


#### UML
Check for image file uml_diagram.png or the uml_diagram.uxf (UMLet Eclipse Plugin)

#### Running the code - Maven Build - Unit Testing - windows
If you have maven open a terminal within the project folder and run **maven.bat**

It will clean, build and run the unit testing

check below (look within the end for **"T E S T S"**):

**> maven.bat**
```
@echo off
echo ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
echo Maven - Build Project - Run Test Cases
echo Please make sure to configure JAVA_HOME
echo Please make sure you have MAVEN and pointed to the PATH
echo ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
pause
echo ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SET "JAVA_HOME=C:\Program Files\Java\jdk1.8.0_102\"
SET "PATH=%PATH%;E:\Dev\apache-maven-3.5.0\bin"
echo ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
CALL mvn clean install
echo ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
pause
```
**> output:**
![unit_testing.png](https://bitbucket.org/braingerm/billing-demo/raw/93a97b203c928125469ae331cd4c0de25df782c1/unit_testing.png)
